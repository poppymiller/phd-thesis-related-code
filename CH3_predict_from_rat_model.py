# The CH3 scripts are designed to be run together
# Libraries, functions and variables may be imported from CH3_rat_model.py

## Create functions
# Define function to calculate predicted values from rat model

def calcEtaR(phidf, sigmasqdf, betadf, tausqdf, Sdf, 
	D, nO, campaign, XP, ids, joint, iters):
	'''phidf, sigmadf, betadf, tausqdf and Sdf are the posterior 
		samples from the rat model,
	D is the distance matrix between prediction points,
	n0 is the line number where the change from Campaign 1 to 2 occurs,
	campaign is the campaign number,
	XP is the model matrix,
	ids lists the locations,
	joint is whether or not to do joint predictions,
	iters is the iteration values at which to do predictions'''
	
	niters = iters.shape[0]
	nT = D.shape[0]
	nP = nT - nO # number of prediction locations
	NP = XP.shape[0] # number of pred data points
	# S has campaign 1 and 2 conjoined. 
	# If campaign 1, then S values are the first nO S's,
	# otherwise they are the last nO S's
	S_ = (Sdf[Sdf.columns[:nO]].copy() if campaign == 0 else 
		Sdf[Sdf.columns[-nO:]].copy()).as_matrix()
	
	SO = np.empty(shape = (niters, nO)) # S observed
	SP = np.empty(shape = (niters, nP)) # S predicted
	lp = np.empty(shape = (niters, NP)) # linear predictor predicted
	pi = np.empty(shape = (niters, NP)) # pi value predicted
	j = 0
	
	for i in np.nditer(iters):
		phi = phidf[i]
		sigmasq = sigmasqdf[i]
		tausq = tausqdf[i]
		Beta = betadf[i].reshape((betadf.shape[1], 1))
		S = SO[j] = S_[i]
		
		const = D * np.sqrt(3) / phi
		Sigma = sigmasq * (1. + const) * np.exp(-const)
		idxO = np.r_[:nO]
		idxP = np.r_[nO:nT]
		SigmaOO = Sigma[np.ix_(idxO, idxO)] + tausq * np.eye(nO, nO)
		SigmaOOInv = np.linalg.inv(SigmaOO)
		SigmaPP = Sigma[np.ix_(idxP, idxP)]
		SigmaPO = Sigma[np.ix_(idxP, idxO)]
		SigmaOP = Sigma[np.ix_(idxO, idxP)]
		
		# prediction type (joint or independent)
		if joint: 
			expectedS = np.dot(np.dot(SigmaPO, SigmaOOInv), S)
			covarianceS = SigmaPP - np.dot(np.dot(SigmaPO, SigmaOOInv),
				 SigmaOP)
			SP[j,] = np.random.multivariate_normal(expectedS,
				 covarianceS, size=1)
		else:
			for k in range(nP):
				SigmaPO_ = SigmaPO[k,:]
				SigmaOP_ = SigmaOP[:,k]
				SigmaPP_ = SigmaPP[k,k]
				expS = np.dot(np.dot(SigmaPO_, SigmaOOInv), S)
				varS = SigmaPP_ - np.dot(np.dot(SigmaPO_, SigmaOOInv),
					 SigmaOP_)
				SP[j, k] = np.random.normal(expS, varS, size = 1)
		lp[j] = np.dot(XP, Beta).flatten() + (SP[j])[ids].flatten()
		pi[j] = inv_cloglog(lp[j])
		j = j + 1
	return pi, lp, SP, SO

## Read in data
# Rat data
dfR = pd.read_csv('rat_data_binom_combined_centred_poly.csv')
(dfR == dfR.sort_values(['campaign', 'id'], ascending=[True, True])).all()
dfR['id'] = dfR['id'] - 1
# Create ids for each unique location in the combined campaigns
dfR = create_idsC12(dfR)
idsC1R, idsC2R, nC1R, nC2R, coordsUC1R, coordsUC2R = 
	create_ids_sep_campaigns(dfR)

# Human data
dfH = pd.read_csv('human_data_rat_covs_centred_poly.csv')
(dfH == dfH.sort_values(['campaign', 'id'], ascending=[True, True])).all()
dfH['id'] = dfH['id'] - 1
# Create ids for each unique location in the combined campaigns
dfH = create_idsC12(dfH)
idsC1H, idsC2H, num_c1_H, num_c2_H, coordsUC1H, coordsUC2H =
	create_ids_sep_campaigns(dfH)

DistRC13d = np.genfromtxt('DistRC13d.csv', delimiter=',')
DistRC23d = np.genfromtxt('DistRC23d.csv', delimiter=',')

import chardet
with open(pathName + '_' + distType + '_' + str(niters) + '/chain-0.csv', 'rb') as f:
	result = chardet.detect(f.read())
postR = pd.read_csv(pathName + '_' + distType + '_' + str(niters) + 
	'/chain-0.csv', encoding = result['encoding'])

Sdf = postR.loc[:, postR.columns.str.startswith('S__')]
betadf = postR.loc[:, postR.columns.str.startswith('beta__')].as_matrix()

phidf = postR.loc[:, postR.columns.str.startswith('phi__')].as_matrix()
sigmasqdf = postR.loc[:, postR.columns.str.startswith('sigmasq__')].as_matrix()
tausqdf = postR.loc[:, postR.columns.str.startswith('tausq__')].as_matrix()

DistPC1 = pd.read_csv('DistRHC13d.csv', header=None)
DistPC2 = pd.read_csv('DistRHC23d.csv', header=None)

dfHC1 = (dfH.where(dfH['campaign'] == 0)).dropna()
dfHC2 = (dfH.where(dfH['campaign'] == 1)).dropna()

## Campaign 1 predictions
PpiC1, PlpC1, PSC1, OSC1 = calcEtaR(
	phidf = phidf,
	sigmasqdf = sigmasqdf,
	betadf = betadf,
	tausqdf = tausqdf,
	Sdf = Sdf,
	D =  DistPC1.as_matrix(),
	nO = DistRC1.shape[0],
	campaign = 0,
	XP = np.matrix(dfHC1[cov_names]),
	ids = idsC1H,
	joint = True,
	iters = np.arange(burnin, niters + burnin, 1) 
	)

## Campaign 2 predictions
PpiC2, PlpC2, PSC2, OSC2 = calcEtaR(
	phidf = phidf,
	sigmasqdf = sigmasqdf,
	betadf = betadf,
	tausqdf = tausqdf,
	Sdf = Sdf,
	D =  DistPC2.as_matrix(),
	nO = DistRC2.shape[0],
	campaign = 1,
	XP = np.matrix(dfHC2[cov_names]),
	ids = idsC2H,
	joint = True,
	iters = np.arange(burnin, niters + burnin, 1)
)
