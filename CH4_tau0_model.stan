functions {
	// Arguments: 
	// initial values:          y0 = {logA, logC}
	// initial time:            t0
	// time vector to solve at: ts
	// parameter values:        params = {beta, delta, rho, gamma}
	// background parameters:   x_r, x_i
	real[] TODE(real t,
	real[] y,
	real[] params,
	real[] x_r,
	int[] x_i) {		
		real dydt[2];
		// Pathogens
		dydt[1] = params[4] - params[2] * exp(y[2]);
		// Cells
		dydt[2] = params[1] * exp(y[1]) - params[3]; 	
		return dydt;
	}
}

data {
	// Titre data
	int<lower = 1> N;               // No. of observations in data
	real<lower = 0> YL[N];          // Data lower
	real<lower = 0> YU[N];          // Data upper
	int cens[N];                    // Data censoring type

	// Time data
	int<lower = 1> D;               // No. of days in time grid ts
	int<lower = 1> time_indices[N]; // Time vector indices
	real ts[D];                     // Time grid

	// ODE data
	int<lower = 1> n_difeq;         // No. of differential equations 
	real<lower = 1> y0[n_difeq];    // Initial number of
	                                //   antibody Cells (C) 
	                                //   and pathogen Antigens (A)

	// Parameter data
	real<lower = 0> sigma;          // sigma
	real<lower = 0> params[4];      // beta, delta, rho, gamma
}

transformed data {
	// For ode solver
	real x_r[0];
	int x_i[0];
	// initial values for ODE on log scale
	real y0log[n_difeq] = log(y0);
}

parameters {
	real<lower = 0> time0;
}

transformed parameters{
	real yl_hat[D, n_difeq];          // Output from the ODE solver
	real<lower = 0> mu[N];            // Fitted values
	real mul[N];                      // Log fitted values
	real<lower = 0> ts0[D];
	
	for (d in 1:D) {
		ts0[d] = ts[d] + time0;
	}
	
	// Solve ode to get fitted values
	yl_hat = integrate_ode_bdf(TODE, y0log, 0.0, ts0, params, x_r, x_i);

	// Get fitted values including random effects
	for (n in 1:N) {
		// y_hat[, , 2] are the fitted titres
		mul[n] = yl_hat[time_indices[n], 2]; 
	}
	mu = exp(mul);
}

model {
	// Priors
	time0 ~ gamma(2, 0.5);   // time 0
	
	// Likelihood
	for (n in 1:N) {
		if (cens[n] == 0) {          // 0: no censoring       
			target += lognormal_lpdf(YL[n] | mul[n], sigma); 
		} else if (cens[n] == 1) {   // 1: right censored                     
			target += lognormal_lccdf(YL[n] | mul[n], sigma); 
		} else if (cens[n] == -1) {  // -1: left censored                    
			target += lognormal_lcdf(YU[n] | mul[n], sigma); 
		} else if (cens[n] == 2) {   // 2: interval censored                    
			target += log_diff_exp(                           
			  lognormal_lcdf(YU[n] | mul[n], sigma), // upper  
			  lognormal_lcdf(YL[n] | mul[n], sigma)  // lower
			);
		}
	}
}

generated quantities {
	vector[N] log_lik;
	for (n in 1:N) {
		if (cens[n] == 0) {         // 0: no censoring                          
			log_lik[n] = lognormal_lpdf(YL[n] | mul[n], sigma); 
		} else if (cens[n] == 1) {  // 1: right censored      
			log_lik[n] = lognormal_lccdf(YL[n] | mul[n], sigma);
		} else if (cens[n] == -1) { // -1: left censored                     
			log_lik[n] = lognormal_lcdf(YU[n] | mul[n], sigma); 
		} else if (cens[n] == 2) {  // 2: interval censored                      
			log_lik[n] = log_diff_exp(                           
			  lognormal_lcdf(YU[n] | mul[n], sigma), // upper  
			  lognormal_lcdf(YL[n] | mul[n], sigma)  // lower
			);
		}
	}
}