	
import os
import errno
import pickle
import numpy as np
import pymc3 as pm
import pandas as pd
import theano as t
import theano.tensor.slinalg as tla
import theano.tensor as tt
from scipy.spatial import distance as dist
import matplotlib.pyplot as plt
import matplotlib
import colorsys
from optparse import OptionParser
from pymc3.backends import Text
from theano.tensor import nlinalg as nla
from pymc3.backends.base import merge_traces
import h5py

pathName = 'RAT_all_covs'
distType = '3d'
nitersR = 10000
burninR = 2000

## Create functions

# inverse cloglog function
def inv_cloglog(x):
res = -tt.expm1(-tt.exp(x))
return res.clip(1.e-9, 1.-1.e-9)

# Create matrix of unique locations
def create_idsC12(data):
	coordsUC12 = data[['xCoord', 'yCoord']].drop_duplicates()
	coordsUC12['id_c12'] = range(coordsUC12.shape[0])
	data = pd.merge(data, coordsUC12, how = 'left')
	return data
	
# Create matrix of location ids for each campaign
def create_ids_sep_campaigns(data):
	# Create vector of ids for each row in each campaign
	dfC1 = (data.where(data['campaign'] == 0)).dropna()
	dfC2 = (data.where(data['campaign'] == 1)).dropna()
	numC1Rows = dfC1.shape[0] # number of rows of data in each campaign
	numC2Rows = dfC2.shape[0]

	# Create matrix of unique coordinates for each campaign
	coordsUC1 = (dfC1[['xCoord', 'yCoord', 'campaign']].drop_duplicates()).dropna()
	coordsUC1['id'] = range(coordsUC1.shape[0])
	coordsUC2 = (dfC2[['xCoord', 'yCoord', 'campaign']].drop_duplicates()).dropna()
	coordsUC2['id'] = range(coordsUC2.shape[0])

	idsC1 = pd.merge(dfC1[['campaign', 'xCoord', 'yCoord']],
		coordsUC1, how = 'left')['id']
	idsC2 = pd.merge(dfC2[['campaign', 'xCoord', 'yCoord']], 
		coordsUC2, how = 'left')['id']
	
	return idsC1, idsC2, numC1Rows, numC2Rows, coordsUC1, coordsUC2

## Define Model

def MaternRat(Y, N, DC1, DC2, X, offset, ids):
	'''	Y is the vector successes,
	DC1 & DC2 are matrices of distances between points for each campaign,
	N is the vector of number of trials,
	X is the model matrix,
	offset is the vector of offsets (number of days exposed),
	ids lists the locations'''
	
	n = N.shape
	N = t.shared(N)
	Y = t.shared(Y)
	offset = t.shared(offset)
	
	# number of spatial random effects in campaigns 1 & 2
	nSC1 = DC1.shape[0]
	nSC2 = DC2.shape[0] 
	
	# distance matrices
	DC1 = t.shared(DC1)
	DC2 = t.shared(DC2)
	
	sqrt3 = tt.constant(np.sqrt(3.))
	
	# covariates
	G = X.shape[1]
	X = t.shared(X)
	
	ids = t.shared(ids)
	
	model = pm.Model()
	
	with model:
		# Priors
		beta = pm.Normal('beta', 0., 100., shape = G)
		tausq = pm.Gamma('tausq', 2., 0.5, shape = 1, testval=0.5)
		sigmasq = pm.Gamma('sigmasq', 2., 0.5, shape = 1, testval=3)
		phi = pm.Gamma('phi', 1.5, 0.05, shape = 1, testval=15)
		
		# Spatial random effects
		constC1 = tt.mul(DC1, sqrt3 / phi)
		constC2 = tt.mul(DC2, sqrt3 / phi)
		
		SigmaSqC1 = sigmasq * (1. + constC1) * tt.exp(-constC1) + 
		tausq * tt.eye(n = nSC1, m = nSC1)
		SigmaSqC2 = sigmasq * (1. + constC2) * tt.exp(-constC2) + 
		tausq * tt.eye(n = nSC2, m = nSC2)
		
		SigmaChC1 = tla.Cholesky(lower=True)(SigmaSqC1) 
		SigmaChC2 = tla.Cholesky(lower=True)(SigmaSqC2)
		sc1 = pm.Normal('sc1', mu=0., sd=1., shape=nSC1)
		sc2 = pm.Normal('sc2', mu=0., sd=1., shape=nSC2)
		Sc1 = tt.dot(SigmaChC1, sc1)
		Sc2 = tt.dot(SigmaChC2, sc2)
		S = pm.Deterministic('S', tt.concatenate((Sc1, Sc2), axis = 0))
		
		# Linear predictor
		eta_R = tt.log(offset) + tt.dot(X, beta) + S[ids]
		
		# Probability
		p = pm.Deterministic('p', inv_cloglog(eta_R))
		
		# Likelihood
		y_obs = pm.Binomial('yobs', n = N, p = p, observed=Y)
		
	return model

## Read in data

dfR = pd.read_csv('rat_data_binom_combined_centred_poly.csv')
# Sort data by campaign and location ids
(dfR == dfR.sort_values(['campaign', 'id'], ascending=[True, True])).all()
dfR['id'] = dfR['id'] - 1 # set id's to have 0 as lowest value
# Create ids for each unique location in the combined campaigns
dfR = create_idsC12(dfR)
idsC1R, idsC2R, nC1R, nC2R, coordsUC1R, coordsUC2R = 
	create_ids_sep_campaigns(dfR)
# Calculate ids for combined rat and human data
ids_R = np.concatenate((idsC1R, idsC2R + idsC1R.max() + 1), axis = 0)
# Read in distances for combined rat and human data
DistRC1 = np.genfromtxt('DistRC13d.csv', delimiter=',')
DistRC2 = np.genfromtxt('DistRC23d.csv', delimiter=',')

## Set up model

cov_names = ['(Intercept)',
	'area_soil_5m', 
	'I(area_soil_5m^2)',
	'area_veg_5m', 
	'I(area_veg_5m^2)',
	'mean_rainfall',
	'dist_3d_public_dump',
	'dist_3d_public_dump_above70m',
	'domestic_11',
	'dist_3d_open_sewer',
	'dist_3d_open_sewer_above40m']

model = MaternRat(
	Y = np.array(dfR[['m']]).flatten(),
	N = np.array(dfR[['n']]).flatten(),
	DC1 = DistRC1,
	DC2 = DistRC2,
	X = np.matrix(dfR[cov_names]),
	offset = np.array(dfR[['time_left']]).flatten(),
	ids = np.array(ids_R).flatten().astype(int))

## Fit model

with model:
db = Text(pathName + distType + '_' + str(niters))
trace = merge_traces([pm.sample(
	draws = niters, 
	tune = burnin, 
	n_init = 200000,
	init = 'advi+adapt_diag',
	start= {'alpha_R': -1.7, 'tau': 0.5,'sigmasq': 3.0,'phi': 15.0},
	trace = db, chain=i)
	for i in range(1)])

