# The CH3 scripts are designed to be run together
# Libraries, functions and variables may be imported from CH3_rat_model.py and CH3_predict_from_rat_model.py

burninH = 2000
nitersH = 500

## Create functions
	
def inv_logit(x):
	return np.exp(x) / (1 + np.exp(x))

def create_ids(data):
	coordsU = data[['indiv_id']].drop_duplicates()
	coordsU['id_c12'] = range(coordsUC12.shape[0])
	data = pd.merge(data, coordsUC12, how = 'left')
	return data
	
# Model
def humanRE(Y, X, id):
	Y = t.shared(Y)
	G = X.shape[1]
	X = t.shared(X)
	n = id.max() + 1
	id = t.shared(id)
	
	model = pm.Model()
	
	with model:
		beta = pm.Normal('beta', 0., 100., shape=G)
		sigma = pm.Gamma('sigma', 2., 0.5, shape = 1)
		z01 = pm.Normal('z01', 0, 1, shape = n)
		z = pm.Deterministic('z', sigma * z01)
		
		eta = tt.dot(X, beta) + z[id]
		p = pm.Deterministic('p', pm.invlogit(eta))
		yObs = pm.Bernoulli('yObs', p=p, observed=Y)	
	return model

## Load data
dfH = pd.read_csv('human_data_rat_covs_centred_poly.csv')
(dfH == dfH.sort_values(['campaign', 'id'], ascending=[True, True])).all()
dfH['indiv_id'] = dfH['indiv_id'] - 1

ratPredEtaC1 = pd.read_csv(pathName + '_' + distType + '_' + str(nitersR) 
	+ '/PredHuman/PlpC1.csv')
ratPredEtaC2 = pd.read_csv(pathName + '_' + distType + '_' + str(nitersR) 
	+ '/PredHuman/PlpC2.csv')

cov_names = ['(Intercept)',
	'dist_3d_open_sewer',
	'dist_3d_public_dump',
	'cum_rainfall',
	'area_soil_5m',
	'area_veg_5m',
	'age_above30',
	'age_below30',
	'sexM',
	'race_combinedBlack',
	'literacyliterate',
	'logincome_above40',
	'logincome_below40',
	'indiv_sewer1',
	'indiv_mud1',
	'indiv_flood1',
	'Rlp']

## Run models

nc = range(0, 1000)
for rowid in nc:
	rowR = rowid * 10 + burninR
	dfH['Rlp'] = np.concatenate((ratPredEtaC1[rowid:(rowid + 1)],
		ratPredEtaC2[rowid:(rowid + 1)]), axis=1).T
	
	model = humanRE(Y=np.array(dfH[['positive_lepto']]).flatten(),
		X=np.matrix(dfH[cov_names]),
		id = np.array(dfH['indiv_id']).flatten())
	
	with model:
		trace = pm.sample(draws=nitersH, tune=burninH, n_init=100000,
			init='advi+adapt_diag')
	
	while any(trace['diverging']):
		with model:
			trace = pm.sample(draws=nitersH, tune=burninH, n_init=100000,
				init='advi+adapt_diag')
	
	tracedf = pm.trace_to_dataframe(trace)
	f = h5py.File(pathName + '_' + distType + '_' + str(nitersR) +
			'/HUMAN_RE_NonCent_' + link + str(rowR) + '_' +
			str(nitersH) + '.hdf5', 'w')
	f.create_dataset('post', data = tracedf)
	f['colNames'] = tracedf.columns.values.astype('S')
	f.close()
