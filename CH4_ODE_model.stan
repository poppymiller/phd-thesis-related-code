functions {
	// Arguments: 
	// initial values:          y0 = {logA, logC}
	// initial time:            t0
	// time vector to solve at: ts
	// parameter values:        params = {beta, delta, rho, gamma}
	// background parameters:   x_r, x_i
	real[] TODE(real t,
	real[] y,
	real[] params,
	real[] x_r,
	int[] x_i) {		
		real dydt[2];
		// Pathogens
		dydt[1] = params[4] - params[2] * exp(y[2]);
		// Cells
		dydt[2] = params[1] * exp(y[1]) - params[3]; 	
		return dydt;
	}
}

data {
	// Titre data
	int<lower = 1> N;               // No. of observations in data
	int<lower = 1> H;               // No. of individuals
	real<lower = 0> YL[N];          // Data lower
	real<lower = 0> YU[N];          // Data upper
	int cens[N];                    // Data censoring type
	
	// Time data
	int<lower = 1> D;               // No. of days in time grid ts
	int<lower = 1> time_indices[N]; // Time vector indices
	real ts[D];                     // Time grid
	
	// ODE data
	int<lower = 1> n_difeq;         // No. of differential equations 
	int<lower = 1> G;               // No. of unique sets 
	//   of ODE parameters
	real<lower = 1> y0[n_difeq];    // Initial number of
	//   antibody Cells (C) 
	//   and pathogen Antigens (A)
	
	// Parameter data
	int<lower = 1> n_beta;          // No. of beta parameters
	int<lower = 1> n_delta;         // No. of delta parameters
	int<lower = 1> n_rho;           // No. of rho parameters
	int<lower = 1> n_gamma;         // No. of gamma parameters
	
	matrix[G, n_beta] beta_mm;      // Model matrix for beta
	matrix[G, n_delta] delta_mm;    // Model matrix for delta
	matrix[G, n_rho] rho_mm;        // Model matrix for rho
	matrix[G, n_gamma] gamma_mm;    // Model matrix for gamma
	
	// Indices
	int<lower = 1> id_indices[N];   // Indices for ID random effects
	int<lower = 1> ode_indices[N];  // Indices for ODE groups
}

transformed data {
	// For ode solver
	real x_r[0];
	int x_i[0];
	// initial values for ODE on log scale
	real y0log[n_difeq] = log(y0);
}

parameters {
	vector<lower = 0>[n_beta] beta;
	vector<lower = 0>[n_delta] delta;
	vector<lower = 0>[n_rho] rho;
	vector<lower = 0>[n_gamma] gamma;
	
	vector[H] zeta;
	
	real<lower = 0> sigma;
	real<lower = 0> sigmaz;
}

transformed parameters{
	real yl_hat[D, G, n_difeq];          // Output from the ODE solver
	real<lower = 0> mu[N];               // Fitted values
	real mul[N];                         // Log fitted values
	real zetan[N];                       // Expanded random effects
	
	// Expanded parameter values
	real<lower = 0> params[4, G]; // parameter values for ODE
	vector<lower = 0>[G] betas  = exp(beta_mm * log(beta));
	vector<lower = 0>[G] deltas = exp(delta_mm * log(delta));
	vector<lower = 0>[G] rhos   = exp(rho_mm * log(rho));
	vector<lower = 0>[G] gammas = exp(gamma_mm * log(gamma));
	
	// Solve ode to get fitted values
	for (g in 1:G) {
		params[1, g] = betas[g];
		params[2, g] = deltas[g];
		params[3, g] = rhos[g];
		params[4, g] = gammas[g];
		// Use stiff solver
		yl_hat[, g, ] = integrate_ode_bdf(TODE, y0log, 0.0, ts, 
			params[, g], x_r, x_i);
	}

	// Get fitted values including random effects
	for (n in 1:N) {
		// random effects for each row of data
		zetan[n] = zeta[id_indices[n]];
		// y_hat[time, group, 2] are the fitted titres
		mul[n] = y_hat[time_indices[n], ode_indices[n], 2] + 
			zetan[n]; 
	}
	mu = exp(mul);
}

model {
	// Priors
	// ODE params
	beta   ~ normal(0, 1);    // half normal
	delta  ~ normal(0, 1);    // half normal
	rho    ~ normal(0, 1);    // half normal  
	gamma  ~ normal(0, 1);    // half normal  
	// SD params
	sigma  ~ gamma(2, 0.5);   // noise sd
	sigmaz  ~ gamma(2, 0.5);  // random effects sd
	// RE params
	zeta ~ normal(0, sigmaz); // random effects
	
	// Likelihood
	for (n in 1:N) {
		if (cens[n] == 0) {              // 0: no censoring       
			target += lognormal_lpdf(YL[n] | mul[n], sigma); 
		} else if (cens[n] == 1) {   // 1: right censored                     
			target += lognormal_lccdf(YL[n] | mul[n], sigma); 
		} else if (cens[n] == -1) {  // -1: left censored                    
			target += lognormal_lcdf(YU[n] | mul[n], sigma); 
		} else if (cens[n] == 2) {   // 2: interval censored                    
			target += log_diff_exp(                           
				lognormal_lcdf(YU[n] | mul[n], sigma), // upper  
				lognormal_lcdf(YL[n] | mul[n], sigma)  // lower
			);
		}
	}
}

generated quantities {
	vector[N] log_lik;
	for (n in 1:N) {
		if (cens[n] == 0) {             // 0: no censoring                          
			log_lik[n] = lognormal_lpdf(YL[n] | mul[n], sigma); 
		} else if (cens[n] == 1) {  // 1: right censored      
			log_lik[n] = lognormal_lccdf(YL[n] | mul[n], sigma);
		} else if (cens[n] == -1) { // -1: left censored                     
			log_lik[n] = lognormal_lcdf(YU[n] | mul[n], sigma); 
		} else if (cens[n] == 2) {  // 2: interval censored                      
			log_lik[n] = log_diff_exp(                           
				lognormal_lcdf(YU[n] | mul[n], sigma), // upper  
				lognormal_lcdf(YL[n] | mul[n], sigma)  // lower
			);
		}
	}
}